import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})



export class AuthService {

  
  constructor(private http: HttpClient) { }

  // uasername, password
  register(user): Promise<any> {
    return this.http.post('https://survey-poodle.herokuapp.com/v1/api/users/register', {
      user: {...user}
    }).toPromise()
  }
  

    private user: any = null;


  /**
   * Attempt to login with given user
   * @param user 
   * @returns boolean
   */
  public login(user: any): boolean {

    if (user.username == 'Oliver' && user.password == 'secret') {
      this.user = {...user};

    } else {
      this.user = null;
    }

    return this.isLoggedIn();
  }

  /**
   * Check if a user is currently logged in
   * @returns boolean
   */
  public isLoggedIn(): boolean {
    return this.user !== null;
  }

}
