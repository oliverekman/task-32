import { Component, Input, Output,  EventEmitter } from '@angular/core';
import { Xtb } from '@angular/compiler';
import { AuthService } from 'src/app/services/auth/auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent {
  
  registerForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(4), Validators.email]),
    password: new FormControl('', [ Validators.required, Validators.pattern('^(?=.*?[A-Z)(?=.*?[a-z])(?=.*?[0-9]).{4,}$')]),
    confirmPassword: new FormControl('', [ Validators.required, Validators.pattern('^(?=.*?[A-Z)(?=.*?[a-z])(?=.*?[0-9]).{4,}$')])
  })



  constructor(private userService: AuthService, private router: Router) { }

  get username() {
    return this.registerForm.get('username')
  }
  get password() {
    return this.registerForm.get('password')
  }
  get confirmPassword() {
    return this.registerForm.get('confirmPassword')
  }
  

  async onRegisterClick() {

    try {
      if (this.password.value !== this.confirmPassword.value) {
        return alert('You have to write the same password smart as...')
      }
      const result: any = await this.userService.register(this.registerForm.value)

      console.log(result);
      if(result) {
        this.router.navigateByUrl('/dashboard')
      }
    } catch (e) {
      console.error(e);
    }
  }



}
