import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms'

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {

  loginForm: FormGroup = new FormGroup({
    username: new FormControl('', [ Validators.required, Validators.minLength(4), Validators.email]),
    password: new FormControl('', [ Validators.required, Validators.minLength(6), Validators.maxLength(20)])
  });


  constructor(private authService: AuthService, private router: Router) { }

  get username() {
    return this.loginForm.get('username')
  }

  get password() {
    return this.loginForm.get('password')
  }



  async onLoginClick() {

    console.log(this.loginForm.value);
    console.log(this.loginForm.valid);

    const success: any = await this.authService.login(this.loginForm.value);
    console.log(success);

    if (success) {
      this.router.navigateByUrl('/dashboard');
    } else {
      alert('Hey, You dont belong here!')
    }


  }

  registerNowClick() {
    this.router.navigateByUrl('/register')
  }


}
